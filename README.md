ics-ans-role-vagrant
===================

Ansible role to install virtualbox and vagrant.

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

Role Variables
--------------

```yaml
virtualbox_version: 5.2
vagrant_version: 2.0.2
vagrant_user: root
vagrant_install_vbguest: True
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-vagrant
```

License
-------

BSD 2-clause
