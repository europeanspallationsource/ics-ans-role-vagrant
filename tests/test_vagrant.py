import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_default(Package):
    assert Package('VirtualBox-5.2').is_installed
    assert Package('vagrant').is_installed


def test_vbguest_installed(Command):
    cmd = Command('vagrant plugin list')
    assert 'vagrant-vbguest' in cmd.stdout


def test_virtualbox(Command):
    cmd = Command('VBoxManage --version')
    # If VirtualBox is not installed properly, we get:
    # WARNING: The vboxdrv kernel module is not loaded...
    assert cmd.stdout.startswith('5.2')
